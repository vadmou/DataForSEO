<?php
require_once('../../components/Db.php');

function getDataString(){
    $db = Db::getConnection();

    $result = $db->query('SELECT ms FROM kwrd_sv ORDER BY id DESC LIMIT 1');

    $row = $result->fetch();

    $response = json_decode($row[0], true);

    $data = '{"cols": [{"label":"Месяц","type":"number"},{"label":"count","type":"number"}],"rows": [';

    foreach ($response as $resp){
        $data .= '{"c":[{"v":'. $resp['month'] . '},{"v":'. $resp['count'] . '}]},';
    }
    $data = rtrim($data, ',');
    $data .= ']}';

    return $data;
}

echo getDataString();