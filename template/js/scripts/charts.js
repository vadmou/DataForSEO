
google.charts.load('current', {'packages':['line']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var jsonData = $.ajax({
        url: "/template/ajax/charts.php",
        dataType: "json",
        async: false
    }).responseText;

    var data = new google.visualization.DataTable(jsonData);

    var options = {
        chart: {
            title: '2016 год',
            },
        width: 900,
        height: 350,
        axes: {
            x: {
                0: {side: 'top'}
            }
        }
    };

    var chart = new google.charts.Line(document.getElementById('line_top_x'));

    chart.draw(data, options);
}