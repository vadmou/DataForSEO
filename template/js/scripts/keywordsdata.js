$(document).ready(function () {

    $('a').button();

    $(".col-md-4").hide().show("slide", 1000);
    $(".col-md-8").hide().show("slide",  {direction: 'up'}, 1000);

    $( "#filtred" ).resizable({
        maxWidth: 800,
        minWidth: 300
    });

    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();

    $( document ).tooltip();


    $("form").validate({
        rules: {
            key: {
                required: true,
                rangelength: [2, 25]
            }
        },
        messages: {
            key: {
                required: "Введите ключевое слово",
                rangelength: "Длина должна быть от {0} до {1} знаков"
            }
        }
    });

    $("input[name = submit]").click(function () {
        var $key = $("input[name = key]").val();

        $.ajax({
            url: "/template/ajax/main.php",
            method: 'post',
            dataType: 'json',
            data: {
                key: $key,
                flag: "2"
            },
            success: function (data) {
                var str = "";
                for (var id in data) {
                    str += "<tr><td>" + data[id]["key"] + "</td>\
					<td>" + data[id]["cmp"] + "</td>\
					<td>" + data[id]["cpc"] + "</td>\
					<td>" + data[id]["sv"] + "</td></tr>";
                }
                $("#filtred tbody").empty().append(str);
                $.getScript('/template/js/scripts/charts.js', function () {
                });
            },
            error: function () {
                console.log("error");
            }
        })
    });
});