$(document).ready(function () {

    $('a').button();

    $(".col-md-4").hide().show("slide", 1000);
    $(".col-md-8").hide().show("slide",  {direction: 'up'}, 1000);

    $( "#filtred" ).resizable({
        maxWidth: 800,
        minWidth: 300
    });

    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();

    $( document ).tooltip();


    $("form").validate({
        rules: {
            post_site: {
                required: true,
                url: true
            },
            post_key: {
                required: true,
                rangelength: [2, 25]
            }
        },
        messages: {
            post_site: {
                required: "Введите URL сайта",
                url: "Неверный формат сайта"
            },
            post_key: {
                required: "Введите ключевое слово",
                rangelength: "Длина должна быть от {0} до {1} знаков"
            }
        }
    });

    $("input[name = submit]").click(function () {
        var $post_site = $("input[name = post_site]").val();
        var $post_key = $("input[name = post_key]").val();

        $.ajax({
            url: "/template/ajax/main.php",
            method: 'post',
            dataType: 'json',
            data: {
                post_site: $post_site,
                post_key: $post_key,
                flag: "1"
            },
            success: function (data) {
                var str = "";
                for (var id in data) {
                    str += "<tr><td>" + data[id]["post_key"] + "</td>\
					<td>" + data[id]["post_site"] + "</td>\
					<td>" + data[id]["result_position"] + "</td>\
					<td>" + data[id]["result_datetime"] + "</td>\
					<td>" + data[id]["task_id"] + "</td></tr>";
                }
                $("#filtred tbody").empty().append(str);
            },
            error: function () {
                console.log("error");
            }
        })
    });
});