<?php

class Ranktracker
{

    /**
     * Get requests from the history table
     * @return array
     */

    public static function getList()
    {
        $db = Db::getConnection();

        $Notes = array();

        $result = $db->query('SELECT post_site, post_key, result_position, result_datetime, task_id FROM ' .
            'rnk_tasks_get ORDER BY id DESC');

        $i = 0;
        while ($row = $result->fetch()) {
            $Notes[$i]['post_site'] = $row['post_site'];
            $Notes[$i]['post_key'] = $row['post_key'];
            $Notes[$i]['result_position'] = $row['result_position'];
            $Notes[$i]['result_datetime'] = $row['result_datetime'];
            $Notes[$i]['task_id'] = $row['task_id'];
            $i++;
        }
        return $Notes;
    }
}