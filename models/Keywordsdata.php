<?php

class Keywordsdata
{

    /**
     * Get requests from the history table
     * @return array
     */

    public static function getList()
    {
        $db = Db::getConnection();

        $Notes = array();

        $result = $db->query('SELECT key, cmp, cpc, sv FROM kwrd_sv ORDER BY id DESC');

        $i = 0;
        while ($row = $result->fetch()) {
            $Notes[$i]['key'] = $row['key'];
            $Notes[$i]['cmp'] = $row['cmp'];
            $Notes[$i]['cpc'] = $row['cpc'];
            $Notes[$i]['sv'] = $row['sv'];
            $i++;
        }

        return $Notes;
    }
}