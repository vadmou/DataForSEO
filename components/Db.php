<?php

class Db
{
    public static function getConnection()
    {

		$params = array(
			'host' => ' ',
			'dbname' => ' ',
			'user' => ' ',
			'password' => ' '
		);

        $dsn = "pgsql:dbname={$params['dbname']};host={$params['host']}";
        $db = new PDO($dsn, $params['user'], $params['password']);

        return $db;
    }
}