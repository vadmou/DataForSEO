<?php

include_once ROOT . '/models/Ranktracker.php';

class RanktrackerController
{

    public function actionInput()
    {

        require_once(ROOT . '/views/ranktracker/input.php');

        return true;
    }

    public function actionView()
    {

        $Notes = "";
        $Notes = Ranktracker::getList();

        require_once(ROOT . '/views/ranktracker/view.php');

        return true;
    }


}
