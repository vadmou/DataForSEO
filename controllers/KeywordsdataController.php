<?php

include_once ROOT . '/models/Keywordsdata.php';

class KeywordsdataController
{
    public function actionInput()
    {
        require_once(ROOT . '/views/keywordsdata/input.php');

        return true;
    }


    public function actionView()
    {
        $Notes = "";
        $Notes = Keywordsdata::getList();

        require_once(ROOT . '/views/keywordsdata/view.php');

        return true;
    }
}
