<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>RankTracker</title>

    <script src="/template/js/jquery-3.1.1.min.js"></script>
    <script src="/template/js/jquery-ui.js"></script>
    <script src="/template/js/scripts/ranktracker.js"></script>
    <script src="/template/js/jquery.validate.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link type="text/css" href="/template/css/jquery-ui.css" rel="stylesheet"/>
    <link type="text/css" href="/template/css/style.css" rel="stylesheet"/>

</head>


<body>
<div class="row">
    <h3>RankTracker - Позиция сайта в google.com.ua</h3>
    <div class="col-md-4">
        <a href="/ranktracker/input">Узнать позицию сайта в поисковом запросе</a><br>
        <a href="/ranktracker/view">История запросов</a><br>
        <br>
        <a href="/keywordsdata/input">KeywordsData</a><br>

    </div>
    <div class="col-md-8">
        <form action="" method="post">
            <input type="text" name="post_site" placeholder="url сайта"
                   title="Введите адрес сайта" class="validate"><br>
            <input type="text" name="post_key" placeholder="ключевое слово"
                   title="Введите ключевое слово" class="validate"><br>
            <input type="submit" value="Поиск" name="submit">
        </form>
        <div class="content">
            <h3>Результат поиска</h3>
            <table id="filtred" class="table table-striped table-bordered table-hover">
                <thead>
                <tr class="column-name">
                    <td>Ключевое слово</td>
                    <td>Адрес сайта</td>
                    <td>Позиция</td>
                    <td>Дата поиска</td>
                    <td>id задачи</td>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>