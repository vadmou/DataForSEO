<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>RankTracker - История запросов</title>

    <script src="/template/js/jquery-3.1.1.min.js"></script>
    <script src="/template/js/jquery-ui.js"></script>
    <script src="/template/js/scripts/ranktracker.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link type="text/css" href="/template/css/jquery-ui.css" rel="stylesheet"/>
    <link type="text/css" href="/template/css/style.css" rel="stylesheet"/>
</head>

<body>
<div class="row">
    <h3>RankTracker - История запросов</h3>
    <div class="col-md-4">
        <a href="/ranktracker/input">Узнать позицию сайта в поисковом запросе</a><br>
        <a href="/ranktracker/view">История запросов</a><br>
        <br>
        <a href="/keywordsdata/input">KeywordsData</a><br>

    </div>
    <div class="col-md-8">
        <div class="content">
            <h3>История запросов</h3>
            <table id="filtred" class="table table-striped table-bordered table-hover">
                <thead>
                <tr class="column-name">
                    <td>Ключевое слово</td>
                    <td>Адрес сайта</td>
                    <td>Позиция</td>
                    <td>Дата поиска</td>
                    <td>id задачи</td>
                </tr>
                </thead>
                <tbody id="sortable">

                <?php foreach ($Notes as $note): ?>
                    <tr>
                        <td><?php echo $note['post_key']; ?></td>
                        <td><?php echo $note['post_site']; ?></td>
                        <td><?php echo $note['result_position']; ?></td>
                        <td><?php echo $note['result_datetime']; ?></td>
                        <td><?php echo $note['task_id']; ?></td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>