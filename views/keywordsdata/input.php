<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>KeywordsData</title>

    <script src="/template/js/jquery-3.1.1.min.js"></script>
    <script src="/template/js/jquery-ui.js"></script>


    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="/template/js/scripts/keywordsdata.js"></script>
    <script src="/template/js/jquery.validate.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link type="text/css" href="/template/css/jquery-ui.css" rel="stylesheet"/>
    <link type="text/css" href="/template/css/style.css" rel="stylesheet"/>

</head>

<body>
<div class="row">
    <h3>KeywordsData - Частота запросов в google</h3>
    <div class="col-md-4">
        <a href="/keywordsdata/input">Узнать частоту запросов ключевого слова</a><br>
        <a href="/keywordsdata/view">История запросов</a><br>
        <br>
        <a href="/ranktracker/input">RankTracker</a><br>

    </div>
    <div class="col-md-8">
        <form method="post" action="javascript:void(null);">
            <input type="text" name="key" placeholder="ключевое слово"
                   title="Введите ключевое слово" class="validate"><br>
            <input type="submit" value="Поиск" name="submit">
        </form>
        <div class="content">
            <h3>Результат поиска</h3>
            <table id="filtred" class="table table-striped table-bordered table-hover">
                <thead>
                <tr class="column-name">
                    <td>Ключевое слово</td>
                    <td>cmp</td>
                    <td>cpc</td>
                    <td>sv</td>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="line_top_x"></div>
    </div>
</div>

</body>
</html>